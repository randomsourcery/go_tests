package main

import (
	"fmt"
)

type request struct {}

type handler func(r request)

func authenticate(r request) (uid int) {
	return 1
}

type withUID func(uid int)

func authMiddleware(next func(r request), wus ...withUID) handler {
	return func(r request) {
		uid := authenticate(r)
		for _, wu := range wus {
			wu(uid)
		}
		next(r)
	}
}

func trace(r request) (traceid string) {
	return "123"
}

type withTraceID func(traceid string)

func traceMiddleware(next func(r request), wts ...withTraceID) handler {
	return func(r request) {
		traceid := trace(r)
		for _, wt := range wts{
			wt(traceid)
		}
		next(r)
	}
}

type bizHandler struct {
	uid     int
	traceid string
}

func (b *bizHandler) withUID(uid int) {
	b.uid = uid
}

func (b *bizHandler) withTraceID(traceid string) {
	b.traceid = traceid
}

func (b *bizHandler) handle(r request) {
	fmt.Printf("%+v\n", r)
	fmt.Printf("%d %s\n", b.uid, b.traceid)
}

type authenticatedHandler func(uid int) handler

type route string

func registerHandler(r route, h handler) {
	fmt.Println(r)
}

func main() {
	h := func(r request) {
		b := &bizHandler{}
		h := traceMiddleware(authMiddleware(b.handle, b.withUID), b.withTraceID)
		h(r)
	}
	h(request{})
	registerHandler("/", h)
}
