/*
Copyright (c) 2009 The Go Authors. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
   * Neither the name of Google Inc. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"fmt"
	"reflect"
)

type Request interface {
	Unwrap() Request
}

// derived from go's errors standard library code
var requestType = reflect.TypeOf((*Request)(nil)).Elem()

func As(r Request, target any) bool {
	if target == nil {
		panic("errors: target cannot be nil")
	}
	val := reflect.ValueOf(target)
	typ := val.Type()
	if typ.Kind() != reflect.Ptr || val.IsNil() {
		panic("errors: target must be a non-nil pointer")
	}
	targetType := typ.Elem()
	if targetType.Kind() != reflect.Interface && !targetType.Implements(requestType) {
		panic("errors: *target must be interface or implement error")
	}
	for r != nil {
		if reflect.TypeOf(r).AssignableTo(targetType) {
			val.Elem().Set(reflect.ValueOf(r))
			return true
		}
		if x, ok := r.(interface{ As(any) bool }); ok && x.As(target) {
			return true
		}
		r = r.Unwrap()
	}
	return false
}
// end of derived code

type handler func(r Request)

type Base struct{}

func (b Base) Unwrap() Request { return nil }

type Authenticated struct {
	uid int
	r   Request
}

func (a Authenticated) UID() int { return a.uid }

func (a Authenticated) Unwrap() Request { return a.r }

func AuthMiddleware(next handler) handler {
	return func(r Request) {
		next(Authenticated{uid: 1, r: r})
	}
}

type Traced struct {
	traceid string
	r       Request
}

func (t Traced) TraceID() string { return t.traceid }

func (a Traced) Unwrap() Request { return a.r }

func TraceMiddleware(next handler) handler {
	return func(r Request) {
		next(Traced{traceid: "123", r: r})
	}
}

func BaseHandler(r Request) {
	fmt.Printf("%+v\n", r)
}

func AuthenticatedTracedHandler(r Request) {
	var a Authenticated
	var t Traced
	As(r, &a)
	As(r, &t)
	fmt.Println(a.UID())
	fmt.Println(t.TraceID())
	fmt.Printf("%+v\n%+v\n%+v\n", a, t, r)
}

func main() {
	handler := TraceMiddleware(AuthMiddleware(AuthenticatedTracedHandler))
	var r Base
	handler(r)
}
